// Bài 1: Tổng các số dương trong mảng
var Z = [-2, -3, -4, -1, 25, 3, 2, 1, 0];
var B = Z.filter(function (n) {
  return n > 0;
});
console.log("B: ", B);
var C = B.reduce(sum, 0);
function sum(total, num) {
  return total + num;
}
console.log("C: ", C);
// Bài 2: Đếm có bao nhiêu số dương trong mảng
var D = B.length;
console.log("D: ", D);
// Bài 3: Tìm số nhỏ nhất trong mảng
//sort(): sort alphabet
//sort(funtion(a-b){return a - b;}): sort number increasing
//sort(funtion(a-b){return b - a;}): sort number decreasing
//spread operator
var E = [...Z].sort(function (a, b) {
  return a - b;
});
console.log("E: ", E);
console.log("E_min: ", E[0]);
// Bài 4: Tìm số nguyên dương nhỏ nhất
var F = B.sort(function (a, b) {
  return a - b;
});
console.log("F_min: ", F[0]);
//Bài 5: Tìm số chẵn cuối cùng trong mảng
var G = Z.filter(function (n) {
  return n % 2 == 0;
});
console.log("G: ", G);
console.log("G_max_so_chan_cuoi_cung: ", G[G.length - 1]);
// Bài 6: Đổi chỗ 2 giá trị trong mảng theo vị trí
var position_1 = 3; //vị trí thứ 1: người dùng nhập
var position_2 = 5; //vị trí thử 2: người dùng nhập
//xem splice, slice
// var Z = [-2, -3, -4, -1, 25, 3, 2, 1, 0];
//
var K = [...Z];
var K_1 = K[5];
var K_2 = K[3];
K.splice(3, 1, K_1);
K.splice(5, 1, K_2);
// console.log("Z: ", Z);
console.log("K: ", K);
//Bài 7: sort(a-b): sắp xếp theo thứ tự tăng dần
console.log("E: ", E);
//Bài 8: xem lại bài nguyên tố, hoặc code lại
//filter số > 2 và là số lẻ;
//ví dụ ra mảng I = [25 3 2 1 0 1 2 3];
var I = [...Z];
console.log("I: ", I);
var b = I.length - 1;
//cho I(i)% (1->i): nếu chia lấy dư = 0, biến đếm count++; nếu count = 2; break; xuất ra số nguyên tố đầu tiên
var count;
var soNguyenToDauTien;
for (var i = 0; i <= b; i++) {
  count = 0;
  for (var j = 1; j <= I[i]; j++) {
    if (I[i] % j === 0) {
      count++;
    }
  }
  if (count === 2) {
    soNguyenToDauTien = I[i];
    break;
  }
}
console.log("soNguyenToDauTien: ", soNguyenToDauTien);

//Bài 9: concat mảng số thực. filter mảng với điều kiện nếu phần tử - Math.floor() của chính nó bằng 0.
Z_float = [1, 2, 3, 5.123, 123.23, 123.2345, 0.2];
Z_mix = [...Z].concat([...Z_float]);
console.log("Z_mix: ", Z_mix);
Z_mix_interger = Z_mix.filter(function (n) {
  return n - Math.floor(n) === 0;
});
console.log("Z_mix_interger: ", Z_mix_interger);
var Z_text = ``;
var a = Z_mix_interger.length - 1;
for (i = 0; i <= a; i++) {
  Z_text += `${Z_mix_interger[i]} `;
}
console.log(`Có ${Z_mix_interger.length} số nguyên trong mảng là: ${Z_text}`);
//Bài 10: so sánh length mảng âm với mảng dương
