var Z = []; //mảng số nguyên
var Z_2 = []; //mảng số thực
var tagInfo = dom_id("info");
var tagResult = dom_id("result");
enter_put_value_into_array("input_array", Z);
enter_put_value_into_array("float_array", Z_2);
// console.log("Z: ", Z);
function confirm_array() {
  if (Z.length === 0) {
    tagInfo.innerHTML = `Vui lòng nhập vào phần tử của mảng`;
  } else {
    var K = [...Z].join(" , ");
    tagInfo.innerHTML = `- Mảng vừa nhập là:  [ ${K} ]`;
  }
}
function confirm_float_array() {
  if (Z_2.length === 0) {
    tagInfo.innerHTML = `Vui lòng nhập vào phần tử của mảng`;
  } else {
    var K = [...Z].concat([...Z_2]);
    var L = K.join(" , ");
    tagInfo.innerHTML = `- Mảng vừa gộp là:  [ ${L} ]`;
    var K_interger = [...K].filter(function (n) {
      return n - Math.floor(n) === 0;
    });
    tagResult.innerHTML = `- Mảng mới có ${K_interger.length} số nguyên`;
  }
}
function reset() {
  //chỗ này không trả mảng về rỗng được, những bài học tiếp theo phải tìm hiểu
  // Z = null; hay Z = [] đều không được, lỗi về biến
  var k = Z.length;
  for (var i = 1; i <= k; i++) {
    Z.pop();
  }
  var tag_float_array = dom_id("float_div_array");
  tag_float_array.classList.add("d-none");
  var tag_replace_index = dom_id("replace_index");
  tagInfo.innerHTML = ``;
  tagResult.innerHTML = ``;
  tag_replace_index.classList.add("d-none");
  return Z;
}
function change_position() {
  var number1 = dom_id_value("index_1");
  var number2 = dom_id_value("index_2");
  var H = [...Z];
  var H_1 = H[number1 - 1];
  var H_2 = H[number2 - 1];
  H.splice(number1 - 1, 1, H_2);
  H.splice(number2 - 1, 1, H_1);
  var L = [...H].join(" , ");
  tagResult.innerHTML = `- Mảng sau khi đổi 2 vị trí  ${number1} và ${number2} là: [ ${L} ] `;
  var tag_replace_index = dom_id("replace_index");
  tag_replace_index.classList.add("d-none");
  console.log("H: ", H);
}
// Feature
function confirm_feature() {
  var select_value = dom_id("select").value * 1;
  console.log("select_value: ", select_value);
  //Bài 1
  if (select_value === 1) {
    var B = Z.filter(function (n) {
      return n > 0;
    });
    console.log("B: ", B);
    var C = B.reduce(sum, 0);
    function sum(total, num) {
      return total + num;
    }
    console.log("C: ", C);
    var L = [...B].join(" + ");
    tagResult.innerHTML = `- Tổng các số dương trong mảng là:  ${L} = ${C} `;
  }
  //Bài 2
  else if (select_value === 2) {
    var B = Z.filter(function (n) {
      return n > 0;
    });
    console.log("B: ", B);
    var D = B.length;
    var L = [...B].join(" , ");
    tagResult.innerHTML = `- Có ${D} số dương trong mảng là:   ${L}`;
    console.log("D: ", D);
  }
  //Bài 3 (How?)
  else if (select_value === 3) {
    var E = [...Z].sort(function (a, b) {
      return a - b;
    });
    console.log("E: ", E);
    console.log("E_min: ", E[0]);
    tagResult.innerHTML = `- Số nhỏ nhất trong mảng là:   ${E[0]}`;
  }
  //Bài 4:
  else if (select_value === 4) {
    var B = Z.filter(function (n) {
      return n > 0;
    });
    var b = B.length;
    if (b === 0) {
      tagResult.innerHTML = `- Không có số dương nào trong mảng`;
    } else {
      var F = B.sort(function (a, b) {
        return a - b;
      });
      console.log("F_min: ", F[0]);
      tagResult.innerHTML = `- Số dương nhỏ nhất trong mảng là:   ${F[0]}`;
    }
  }
  //Bài 5:
  else if (select_value === 5) {
    var G = Z.filter(function (n) {
      return n % 2 == 0;
    });
    console.log("G: ", G);
    console.log("G_max_so_chan_cuoi_cung: ", G[G.length - 1]);
    tagResult.innerHTML = `- Số chẵn cuối cùng trong mảng là: ${
      G[G.length - 1]
    }`;
  }
  // Bài 6:
  else if (select_value === 6) {
    var tag_replace_index = dom_id("replace_index");
    tag_replace_index.classList.remove("d-none");
    tag_replace_index.className = "show";
  }
  // Bài 7:
  else if (select_value === 7) {
    var E = [...Z].sort(function (a, b) {
      return a - b;
    });
    console.log("E: ", E);
    var L = [...E].join(" , ");
    tagResult.innerHTML = `- Mảng theo thứ tự tăng dần là là: [ ${L} ]`;
  }
  // Bài 8:
  else if (select_value === 8) {
    var B = Z.filter(function (n) {
      return n >= 2;
    });
    var I = [...B];
    console.log("I =: ", I);
    var b = I.length - 1;
    var count;
    var soNguyenToDauTien;
    if (b < 0) {
      tagResult.innerHTML = `-1`;
    } else {
      for (var i = 0; i <= b; i++) {
        count = 0;
        for (var j = 1; j <= I[i]; j++) {
          if (I[i] % j === 0) {
            count++;
          }
        }
        if (count === 2) {
          soNguyenToDauTien = I[i];
          tagResult.innerHTML = `- Số nguyên tố đầu tiên trong mảng là:   ${soNguyenToDauTien}`;
          break;
        } else {
          tagResult.innerHTML = `-1`;
        }
      }
    }
  }
  //Bài 9:
  else if (select_value === 9) {
    var tag_float_array = dom_id("float_div_array");
    tag_float_array.classList.remove("d-none");
    tag_float_array.className = "show";
  }
  //Bài 10:
  else if (select_value === 10) {
    var K = [...Z].join(" , ");
    var B = Z.filter(function (n) {
      return n > 0;
    });
    var C = Z.filter(function (n) {
      return n < 0;
    });
    var b = B.length;
    var c = C.length;
    if (b > c) {
      tagInfo.innerHTML = `- Mảng vừa nhập là:  [ ${K} ]`;
      tagResult.innerHTML = `Số lượng số dương lớn hơn số âm (${b} so với ${c})`;
    } else if (b === c) {
      tagInfo.innerHTML = `- Mảng vừa nhập là:  [ ${K} ]`;
      tagResult.innerHTML = `Số lượng số dương bằng số âm (${b} so với ${c})`;
    } else {
      tagInfo.innerHTML = `- Mảng vừa nhập là:  [ ${K} ]`;
      tagResult.innerHTML = `Số lượng số dương nhỏ hơn số âm (${b} so với ${c})`;
    }
  }
}
